﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader Instance { get; private set; }
    public bool willLoad;
    public string deviceId;
    public int mapId;
    public int themeId;
    public static bool isRun;
    public bool isTempData;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        isRun = false;

        DontDestroyOnLoad(this);

    }
    void Start()
    {
        DontDestroyOnLoad(this);
        deviceId = SystemInfo.deviceUniqueIdentifier;
    }

    public void LoadMainScene(bool isNew = false)
    {
        StartCoroutine(CRLoadMainScene(1, isNew));
    }
    public void LoadLobyScene()
    {
        StartCoroutine(CRLoadMainScene(0, false));
    }

    IEnumerator CRLoadMainScene(int scene, bool isNew)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        while (!async.isDone)
        {
            isRun = true;
            // Debug.Log("loading");
            yield return null;
        }
        isRun = false;
        if (scene == 0)
        {
            GameObject.FindObjectOfType<RedisManager>().isLoaded = false;
        }
        
        GvrViewer.Instance.VRModeEnabled = VRModeToggle.Instance.VRMode;
        GameObject.FindObjectOfType<RedisManager>().isNotStart = true;
        if (TutorialManager.Instance.isFirst && scene == 1) TutorialManager.Instance.DoMission(2);
        AudioManager.Instance.PlaySound(0);

        yield return null;
        if (scene == 1 && isNew)
        {
            
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Korean:
                    GameObject.FindGameObjectWithTag("Node").GetComponentInChildren<Text>().text = "제목 없음";
                    
                    break;
                case SystemLanguage.Chinese:
                    GameObject.FindGameObjectWithTag("Node").GetComponentInChildren<Text>().text = "Untitled";
                    break;
                case SystemLanguage.Japanese:
                    GameObject.FindGameObjectWithTag("Node").GetComponentInChildren<Text>().text = "Untitled";
                    break;
                default:
                    GameObject.FindGameObjectWithTag("Node").GetComponentInChildren<Text>().text = "Untitled";
                    break;

            }

        }
    }


}
