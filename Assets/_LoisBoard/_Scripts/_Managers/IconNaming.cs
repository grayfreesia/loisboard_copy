﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconNaming : MonoBehaviour
{
    public enum IconName
    {
        CreateSpace,
        UnsavedSpace,
        Entrance,
        EditPage,
        EditCancle,
        Tutorial,
        Recenter,
        Here,
        Mother,
        Exit,
        Undo,
        Delete,
        Save,
        Export,
        Home,
        Skip
    }
    public IconName icon;
    
    void Start()
    {
        switch(icon)
        {
            case IconName.CreateSpace:
                GetComponent<Text>().text = LanguageManager.Instance.iconCreateSpace;
                break;
            case IconName.UnsavedSpace:
                GetComponent<Text>().text = LanguageManager.Instance.iconUnsavedSpace;
                break;
            case IconName.Entrance:
                GetComponent<Text>().text = LanguageManager.Instance.iconEntrance;
                break;
            case IconName.EditPage:
                GetComponent<Text>().text = LanguageManager.Instance.iconEditPage;
                break;
            case IconName.EditCancle:
                GetComponent<Text>().text = LanguageManager.Instance.iconEditCancle;
                break;
            case IconName.Tutorial:
                GetComponent<Text>().text = LanguageManager.Instance.iconTutorial;
                break;
            case IconName.Recenter:
                GetComponent<Text>().text = LanguageManager.Instance.iconRecenter;
                break;
            case IconName.Here:
                GetComponent<Text>().text = LanguageManager.Instance.iconHere;
                break;
            case IconName.Mother:
                GetComponent<Text>().text = LanguageManager.Instance.iconMother;
                break;
            case IconName.Exit:
                GetComponent<Text>().text = LanguageManager.Instance.iconExit;
                break;
            case IconName.Undo:
                GetComponent<Text>().text = LanguageManager.Instance.iconUndo;
                break;
            case IconName.Delete:
                GetComponent<Text>().text = LanguageManager.Instance.iconDelete;
                break;
            case IconName.Save:
                GetComponent<Text>().text = LanguageManager.Instance.iconSave;
                break;
            case IconName.Export:
                GetComponent<Text>().text = LanguageManager.Instance.iconExport;
                break;
            case IconName.Home:
                GetComponent<Text>().text = LanguageManager.Instance.iconHome;
                break;
            //case IconName.Skip:
            //    GetComponent<Text>().text = LanguageManager.Instance.iconSkip;
            //    break;
            default:
                break;
        }
        
    }
}
