﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager Instance;

    public string pannelTitle;
    public string pannelProgressing;
    public string pannelFailRecognition;

    public string popupUndoSuccess;
    public string popupDeleteSuccess;
    public string popupSaveSuccess;
    public string popupuExportProgressing;
    public string popupExportSuccess;
    public string popupNoSpace2Delete;
    public string popupEditComplete;
    public string popupEditMode;

    public string iconCreateSpace;
    public string iconUnsavedSpace;
    public string iconEntrance;
    public string iconEditPage;
    public string iconEditCancle;
    public string iconTutorial;
    public string iconRecenter;
    public string iconHere;
    public string iconMother;
    public string iconExit;
    public string iconUndo;
    public string iconDelete;
    public string iconSave;
    public string iconExport;
    public string iconHome;
    //public string iconSkip;

    public string[] tutorialText = new string [15];

    void Awake()
    {
        if (Instance == null) Instance = this;
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Korean:
                pannelTitle = "제목 없음";
                pannelProgressing = "인식중...";
                pannelFailRecognition = "다시 말씀해 주세요";
                popupUndoSuccess = "실행 취소";
                popupDeleteSuccess = "모든 노드 삭제";
                popupSaveSuccess = "저장 성공";
                popupuExportProgressing = "캡쳐중...";
                popupExportSuccess = "캡쳐 성공";
                popupNoSpace2Delete = "더 이상 지울 공간이 없습니다";
                popupEditComplete = "편집 완료";
                popupEditMode = "페이지 편집";
                iconCreateSpace = "새 공간 만들기";
                iconUnsavedSpace = "저장되지 않은 공간!";
                iconEntrance = "들어가기";
                iconEditPage = "페이지 편집";
                iconEditCancle = "편집 취소";
                iconTutorial = "튜토리얼";
                iconRecenter = "재배치";
                iconHere = "현 위치";
                iconMother = "첫 노드";
                iconExit = "나가기";
                iconUndo = "실행 취소";
                iconDelete = "모두 삭제";
                iconSave = "저장하기";
                iconExport = "캡쳐하기";
                iconHome = "홈으로";
                //iconSkip = "튜토리얼 끝내기";


                tutorialText[0] = "네트워크에 연결되지 않았습니다.\n로이스보드를 사용하기 전 네트워크를 확인해주세요.";
                tutorialText[1] = "<color=#fa7071ff>위치 변경</color> 아이콘을 바라보세요.";
                tutorialText[2] = "더하기 버튼을 바라보면 첫 번째 공간이 만들어져요.\n공간의 테마는 만든 시간에 따라 다양하게 나타나요.";
                tutorialText[3] = "첫 노드를 바라보면 버튼이 펼쳐져요.";
                tutorialText[4] = "<color=#0084ebff>마이크 버튼</color>을 2초간 바라보고 \n쓰고 싶은 걸 말해보세요.";
                tutorialText[5] = "입력 시간은 최대 7초에요.\n말을 다 했으면 <color=#0084ebff>정지 버튼</color>을 바라보세요.";
                tutorialText[6] = "잘했어요! 첫 노드를 완성했어요.\n첫 노드가 이 공간의 이름이에요..";
                tutorialText[7] = "<color=#85e9beff>추가 버튼</color>을 눌러서 다른 노드를 만들어보세요";
                tutorialText[8] = "잘했어요! 두 번째 노드를 완성했어요.";
                tutorialText[9] = "이 노드를 삭제하려면 <color=#fa7071ff>x 버튼</color>을 바라보세요.";
                tutorialText[10] = "아래로 고개를 내려서 <color=#c1fa70ff>저장 버튼</color>을 바라보세요.\n이 공간이 홈에 저장될 거예요.";
                tutorialText[11] = "최고에요!\n이제 <color=#c1fa70ff>360 캡쳐 버튼</color>을 바라보세요.";
                tutorialText[12] = "잘했어요!\n갤러리 폴더에 이 공간의 <color=#c1fa70ff>파노라마 사진</color>이 저장되었어요.\nSNS를 통해 360도 캡쳐 사진을 친구들과 공유해보세요!";
                tutorialText[13] = "아래에 있는 <color=#c1fa70ff>홈으로</color> 버튼을 바라보세요.\n홈으로 가면 저장한 공간을 볼 수 있어요.";
                tutorialText[14] = "축하해요!\n튜토리얼을 성공적으로 마쳤어요.\n<color=#c1fa70ff>이제 자신만의 브레인스토밍 공간을 만들어보세요!</color>";
                break;

            case SystemLanguage.Japanese:
                pannelTitle = "タイトル無し";
                pannelProgressing = "認識中...";
                pannelFailRecognition = "もう一度話して下さい!";
                popupUndoSuccess = "キャンセルしました";
                popupDeleteSuccess = "Nodeを全て削除!";
                popupSaveSuccess = "保存成功！";
                popupuExportProgressing = "キャプチャー中...";
                popupExportSuccess = "キャプチャー成功！";
                popupNoSpace2Delete = "これ以上空間がありません";
                popupEditComplete = "編集完了！";
                popupEditMode = "ページ編集モード";
                iconCreateSpace = "新しい空間の作成";
                iconUnsavedSpace = "保存していない空間";
                iconEntrance = "入る";
                iconEditPage = "ページ編集";
                iconEditCancle = "ページ削除";
                iconTutorial = "チュートリアル";
                iconRecenter = "現在地変更";
                iconHere = "現在地";
                iconMother = "1つ目のnode";
                iconExit = "終了";
                iconUndo = "キャンセル";
                iconDelete = "全て削除";
                iconSave = "保存";
                iconExport = "キャプチャー";
                iconHome = "ホームへ";
                //iconSkip = "スキップ";

                tutorialText[0] = "ネットワークの接続がありません。\nロイスボードを使用する前にネットワークを確認して下さい。";
                tutorialText[1] = "<color=#fa7071ff>位置変更</color>アイコンを見詰めて下さい。";
                tutorialText[2] = "プラスボタンを見つめながら１つ目の空間を作成していきます。\n空間のテーマは作成する時間によって違います。";
                tutorialText[3] = "１つ目のnodeを見つめるとボタンが広がります。";
                tutorialText[4] = "<color=#0084ebff>マイクボタン</color>を２秒間見つめて話してみて下さい。";
                tutorialText[5] = "入力時間は最大７秒です。\n話し終えたら<color=#0084ebff>停止ボタン</color>を見つめて下さい。";
                tutorialText[6] = "よくできました！１つ目のnodeが完成しました。\n初めてのnodeがこの空間のタイトルです。";
                tutorialText[7] = "<color=#85e9beff>プラスボタン</color>を押して、他のnodeを作ってみましょう。";
                tutorialText[8] = "よくできました！２つめのnodeが完成しました。";
                tutorialText[9] = "このnodeを削除するなら<color=#fa7071ff>☓ボタン</color>を見つめてください。";
                tutorialText[10] = "下の<color=#c1fa70ff>保存ボタン</color>を見つめて下さい。\nこの空間がホームに保存されます。";
                tutorialText[11] = "ナイス！\n次に、<color=#c1fa70ff>360度キャプチャーボタン</color>を見つめて下さい。";
                // tutorialText[12] = "よくできました！\nギャラリーフォルダーにこの空間の<color=#c1fa70ff>パノラマ写真</color>が保存されました。\nSNSを通じて360度のキャプチャー写真を友達と共有してみましょう！";
                tutorialText[12] = "よくできました！\nギャラリーに<color=#c1fa70ff>パノラマ写真</color>が保存されました。\nキャプチャーした写真をSNSでを友達と共有してみましょう！";
                tutorialText[13] = "下にある<color=#c1fa70ff>ホームへ</color>ボタンを見つめて下さい。\nホームへ戻ると保存した空間を見ることができます。";
                // tutorialText[14] = "おめでとうございます！\nチュートリアルを終えました。\n<color=#c1fa70ff>さあ、これから自分だけのブレインストーミング空間を作ってみましょう！</color>";
                tutorialText[14] = "おめでとうございます。\nチュートリアルを終えました。\n<color=#c1fa70ff>さあ、自分だけの空間を作ってみましょう！</color>";
                break;

            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
            case SystemLanguage.ChineseTraditional:
                pannelTitle = "未命名";
                pannelProgressing = "处理中.. ";
                pannelFailRecognition = "未识别出文字";
                popupUndoSuccess = "撤销成功!";
                popupDeleteSuccess = "删除成功!";
                popupSaveSuccess = "保存成功!";
                popupuExportProgressing = "导出中...";
                popupExportSuccess = "导出成功!";
                popupNoSpace2Delete = "没有可以删除的空间";
                popupEditComplete = "编程完成!";
                popupEditMode = "编辑模式";
                iconCreateSpace = "创建一个新空间";
                iconUnsavedSpace = "未保存的空间!";
                iconEntrance = "入口";
                iconEditPage = "编辑页面";
                iconEditCancle = "取消编辑";
                iconTutorial = "引导";
                iconRecenter = "重新居中";
                iconHere = "现在位置";
                iconMother = "主节点";
                iconExit = "退出";
                iconUndo = "撤销 ";
                iconDelete = "删除";
                iconSave = "保存";
                iconExport = "截图";
                iconHome = "首页";
                //iconSkip = "건너뛰기";

                tutorialText[0] = "未联网\n请检测您的网络";
                tutorialText[1] = "请低头凝视<color=#fa7071ff>居中按钮</color>";
                tutorialText[2] = "请凝视“加号”新建一个空间\n\n新建空间的主题根据时间而变\n";
                tutorialText[3] = "请凝视主节点展开按钮";
                tutorialText[4] = "<color=#0084ebff>请凝视 麦克按钮</color>2秒\n说出想写的内容\n\n语言识别可以转换成文字";
                tutorialText[5] = "最多可以说7秒\n\n如果需要停止\n请凝视<color=#0084ebff>停止按钮</color>";
                tutorialText[6] = "太棒了！您的第一个节点已创建\n它的内容将成为这个空间的题目";
                tutorialText[7] = "请凝视<color=#85e9beff>加号</color>添加新的节点\n";
                tutorialText[8] = "棒极了，您成功创建了第二个节点\n请凝视第二个节点白球，展开按钮";
                tutorialText[9] = "请凝视<color=#fa7071ff>删除按钮</color>，删除该节点";
                tutorialText[10] = "接下来，请低头凝视<color=#c1fa70ff>保存按钮n</color>\n可以保存至首页";
                tutorialText[11] = "好样的！\n接下来，请低头凝视360度<color=#c1fa70ff>截图按钮</color>";
                tutorialText[12] = "好样的！你已经成功将这个空间\n导出为<color=#c1fa70ff>全景图片</color>并保存在相册";
                tutorialText[13] = "请凝视<color=#c1fa70ff>首页</color>按钮\n返回首页后,\n可以查看您创建的空间";
                tutorialText[14] = "祝贺！您已完成使用指导\n<color=#c1fa70ff>现在可以开始创建自己的空间了.</color>";
               
                break;

            case SystemLanguage.English:
            default:
                pannelTitle = "Untitled";
                pannelProgressing = "Processing.. ";
                pannelFailRecognition = "No word detected !";
                popupUndoSuccess = "Undo Successfully !";
                popupDeleteSuccess = "Deleted Successfully !";
                popupSaveSuccess = "Saved Successfully !";
                popupuExportProgressing = "Export Progressing...";
                popupExportSuccess = "Export Successfully !";
                popupNoSpace2Delete = "there are no space to delete!";
                popupEditComplete = "Edit complete!";
                popupEditMode = "Edit page mode";
                iconCreateSpace = "Create new space";
                iconUnsavedSpace = "Unsaved \"Space\"!";
                iconEntrance = "Entrance";
                iconEditPage = "EDIT PAGE";
                iconEditCancle = "EDIT CANCLE";
                iconTutorial = "TUTORIAL";
                iconRecenter = "RECENTER";
                iconHere = "HERE";
                iconMother = "MOTHER";
                iconExit = "EXIT";
                iconUndo = "UNDO";
                iconDelete = "DELETE";
                iconSave = "SAVE";
                iconExport = "EXPORT";
                iconHome = "HOME";
                //iconSkip = "SKIP";


                tutorialText[0] = "It seems like you are not connected\nto an availabe network.\n\nPlease check your network before using Loisboard.";
                tutorialText[1] = "Look down to start with gazing <color=#fa7071ff>re-center icon</color>";
                tutorialText[2] = "Look at the \"Plus\" button to create your first space\n\nThe theme of the space you created might be various\ndepending on the time you created it.";
                tutorialText[3] = "Look up the mother node to spread buttons";
                tutorialText[4] = "Look at the <color=#0084ebff>mic button</color> for 2 seconds\nand speak out what you want to write.\n\nVoice recognition will turn it into text";
                tutorialText[5] = "You can speak up to 7 seconds.\n\nif you want to stop,\nplease look at the <color=#0084ebff>stop button</color>.";
                tutorialText[6] = "Great job! Your first node is done.\nIt will be the title of this space.";
                tutorialText[7] = "Look at the <color=#85e9beff>plus button</color> to create another node.";
                tutorialText[8] = "Excellent. You just created the second node.";
                tutorialText[9] = "Look at the <color=#fa7071ff>x button</color> to delete this node.";
                tutorialText[10] = "Next please look down and look at the <color=#c1fa70ff>save button</color>\nyou can save what you've created to home.";
                tutorialText[11] = "Nice job!\nNext, look at the 360 <color=#c1fa70ff>screenshot button</color>";
                tutorialText[12] = "Good job! You've just exported this space\nas a <color=#c1fa70ff>panoramic image</color> to your local gallery.\n\nYou can share it with your friends thorough social media!";
                tutorialText[13] = "Now please look at <color=#c1fa70ff>home</color> at the bottom.\nWhen you go back home,\nyou will see the space you saved.";
                tutorialText[14] = "Congratulation!\nYou've completed tutorials successfully.\n<color=#c1fa70ff>Now you can make your own brainstorming space.</color>";
                break;
        }

    }




    public FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators.LanguageCode LanguageTran()
    {
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Korean:
                return FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators.LanguageCode.KO;

            case SystemLanguage.English:
                return FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators.LanguageCode.EN_US;

            case SystemLanguage.Japanese:
                return FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators.LanguageCode.JA;

            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
            case SystemLanguage.ChineseTraditional:
                return FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators.LanguageCode.ZH_TW;

            default:
                return FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators.LanguageCode.EN_US;

        }
    }



}
