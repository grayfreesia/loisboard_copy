﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TutorialManager : MonoBehaviour
{
    public static TutorialManager Instance;
    public GameObject missionCanvas;


    public GameObject _currentMissionCanvas;
    public GameObject warning;

    public int currentMissionNum = 0;
    public GameObject tutorialImageSet;
    public GameObject tutorialCanvasPositionSet;

    public RectTransform[] tutorialCanvasPositions;
    

    private Image[] _tutorialImages;
    public bool isFirst;

    private Transform _newNodePos;


    public GameObject pointerArrow;
    public GameObject currentPointerArrow;
    public GameObject tutorialPointerPositionSet;
    public RectTransform[] tutorialPointerPositions;
    public bool hoverErrorProtect;

    GameObject[] allNode;
    GameObject lastNode;

    public GameObject tutorialTextPositionSet;
    public RectTransform[] tutorialTextPositions;

    public GameObject tutorialSkipPositionSet;
    public RectTransform[] tutorialSkipPositions;

    void Awake()
    {
        if (Instance == null) Instance = this;
        DontDestroyOnLoad(this);

        tutorialCanvasPositions = tutorialCanvasPositionSet.GetComponentsInChildren<RectTransform>();
        tutorialPointerPositions = tutorialPointerPositionSet.GetComponentsInChildren<RectTransform>();
        tutorialTextPositions = tutorialTextPositionSet.GetComponentsInChildren<RectTransform>();
        tutorialSkipPositions = tutorialSkipPositionSet.GetComponentsInChildren<RectTransform>();
        _tutorialImages = tutorialImageSet.GetComponentsInChildren<Image>();

        //test
        if (PlayerPrefs.GetInt("isFirst", 1) == 1)
        {
            isFirst = true;
            PlayerPrefs.SetInt("isFirst", 0);
            PlayerPrefs.Save();
        }
        else
        {
            isFirst = false;
            //test용 
            //PlayerPrefs.SetInt("isFirst", 1);
            //PlayerPrefs.Save();
            //
        }

        //isFirst = true;

    }
    void Start()
    {
        if (isFirst) DoMission(0);
    }

    public void StartTutorial()
    {
        if (currentMissionNum != 14)
        {
            isFirst = true;
            currentMissionNum = 0;
            DoMission(0);
        }
    }

    public void DoMission(int missionNum)
    {
        //if user has complete current tutorial mission, load next tutorial canvas
        if (currentMissionNum == missionNum) StartCoroutine(CRDoMission(missionNum));
        if(currentMissionNum == 1 && missionNum == 2) 
        {
            currentMissionNum ++;
             StartCoroutine(CRDoMission(missionNum));
        }
    }
    public IEnumerator NetworkWarning()
    {
        //if(warning != null) Destroy(warning.gameObject);
        //yield return new WaitForSeconds(1f);
        if(warning == null)
        {
            warning = Instantiate(missionCanvas, Vector3.zero, Quaternion.identity);
            
            GameObject temp = new GameObject("temp");
            temp.transform.position = Camera.main.transform.position;
            temp.transform.rotation = Quaternion.Euler(0f, Camera.main.transform.rotation.y * 120f, 0f);

            warning.transform.parent = temp.transform;
            warning.transform.localPosition = Vector3.forward * 6f;
            //_curruntMissionCanvas.transform.localRotation = tutorialCanvasPositions[curruntMissionNum].rotation;
            warning.transform.parent = Camera.main.transform.parent;
            warning.transform.LookAt(Camera.main.transform);
            warning.transform.Rotate(0f, 180f, 0f, Space.Self);
            warning.GetComponentInChildren<Image>().sprite = _tutorialImages[0].sprite;
            AudioManager.Instance.PlaySound(12);
            warning.GetComponentInChildren<Text>().text = LanguageManager.Instance.tutorialText[0];
            warning.GetComponentInChildren<Text>().transform.localPosition = new Vector3(0f,-50f,0f);

            Destroy(warning.transform.GetChild(2).gameObject);
            yield return new WaitForSeconds(3f);
            for (int i = 0; i < 100; i++)
            {
                warning.GetComponentInChildren<Image>().color = new Color(1f, 1f, 1f, 1f - 0.01f * i);
                warning.GetComponentInChildren<Text>().color = new Color(1f, 1f, 1f, 1f - 0.01f * i);
                yield return null;
            }

            Destroy(warning.gameObject);
        }
    }
    IEnumerator CRDoMission(int missionNum)
    {
        //after finish load scene
        while (SceneLoader.isRun)
        {
            yield return null;
        }
       
        //delete previous canvas
        if (_currentMissionCanvas != null) Destroy(_currentMissionCanvas);

        //for recenter tutorial// fake camra rotation
        // if(currentMissionNum == 1) Camera.main.transform.parent.transform.rotation = Quaternion.Euler(0f, 60f, 0f);
        // else if (currentMissionNum == 2) Camera.main.transform.parent.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        currentMissionNum++;

        if (currentMissionNum < tutorialCanvasPositions.Length)
        {
            //get next tutorial position
            _currentMissionCanvas = Instantiate(missionCanvas, tutorialCanvasPositions[currentMissionNum].position, tutorialCanvasPositions[currentMissionNum].rotation);

            if (currentMissionNum == 1 || (currentMissionNum > 9 && currentMissionNum < 14))//relative
            {

                GameObject temp = new GameObject("temp");
                temp.transform.position = Camera.main.transform.position;
                temp.transform.rotation = Quaternion.Euler(0f, Camera.main.transform.rotation.y * 120f, 0f);

                _currentMissionCanvas.transform.parent = temp.transform;
                _currentMissionCanvas.transform.localPosition = tutorialCanvasPositions[currentMissionNum].position;
                //_curruntMissionCanvas.transform.localRotation = tutorialCanvasPositions[curruntMissionNum].rotation;
                _currentMissionCanvas.transform.parent = Camera.main.transform.parent;


            }
            else if (currentMissionNum == 8 || currentMissionNum == 9)//new node
            {
                //Debug.Log("th");
                //Debug.Log(_newNodePos);
                allNode = GameObject.FindGameObjectsWithTag("Node");
                lastNode = allNode[allNode.Length - 1];
                allNode = null;
                if (lastNode == null)
                {
                    lastNode = new GameObject();
                    lastNode.transform.position = Vector3.zero;
                }
                _currentMissionCanvas.transform.localPosition = tutorialCanvasPositions[currentMissionNum].position + new Vector3(lastNode.transform.position.x * 1f, lastNode.transform.position.y * 1f, (-13f + lastNode.transform.position.z) * 1f);
                //_curruntMissionCanvas.transform.localRotation =   _newNodePos.rotation;
            }
            //get next tutorial image
            _currentMissionCanvas.transform.LookAt(Camera.main.transform);
            _currentMissionCanvas.transform.Rotate(0f, 180f, 0f, Space.Self);
            _currentMissionCanvas.GetComponentInChildren<Image>().sprite = _tutorialImages[currentMissionNum].sprite;
            _currentMissionCanvas.GetComponentInChildren<Image>().SetNativeSize();
            if (currentMissionNum == 14)
            {
                StartCoroutine(EndTutorial());
            }

            PointArrow();
            _currentMissionCanvas.GetComponentInChildren<Text>().text = LanguageManager.Instance.tutorialText[currentMissionNum];
            _currentMissionCanvas.GetComponentInChildren<Text>().transform.localPosition = tutorialTextPositions[currentMissionNum].position;
            _currentMissionCanvas.transform.GetChild(2).transform.localPosition = tutorialSkipPositions[currentMissionNum].position;
        }
        else
        {

        }
    }

    public IEnumerator NextTutorial(int num, float time)
    {
        yield return new WaitForSeconds(time);
        DoMission(num);
        //yield return new WaitForSeconds(1f);
        if (!hoverErrorProtect)
        {
            hoverErrorProtect = !hoverErrorProtect;
            PointArrowHover(true);
            
        }
    }

    public IEnumerator EndTutorial()
    {
        if (currentMissionNum == 14)
        {
            Destroy(_currentMissionCanvas.transform.GetChild(2).gameObject);
            yield return new WaitForSeconds(5f);
            for (int i = 0; i < 100; i++)
            {
                if (_currentMissionCanvas == null)
                {
                    ArrowProgress.Instance.directionCanvas.transform.position = Vector3.zero;
                    yield break;
                }
                _currentMissionCanvas.GetComponentInChildren<Image>().color = new Color(1f, 1f, 1f, 1f - 0.01f * i);
                _currentMissionCanvas.GetComponentInChildren<Text>().color = new Color(1f, 1f, 1f, 1f - 0.01f * i);
                yield return null;
            }
        }
        PlayerPrefs.SetInt("isFirst", 0);
        PlayerPrefs.Save();
        isFirst = false;
        currentMissionNum = 0;
        ArrowProgress.Instance.directionCanvas.transform.position = Vector3.zero;
        Destroy(_currentMissionCanvas.gameObject);

    }
    public void GetNewNodePosition(Transform pos)
    {
        _newNodePos = pos;
    }

    public void PointArrow()
    {
        if (currentPointerArrow != null) Destroy(currentPointerArrow);
        currentPointerArrow = Instantiate(pointerArrow);
        switch(currentMissionNum)
        {
            
            case 1:
              
                currentPointerArrow.transform.parent = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<RecenterMiniMap>().transform.GetChild(1).transform;
                currentPointerArrow.transform.localPosition = tutorialPointerPositions[currentMissionNum].position;
                
                currentPointerArrow.transform.localRotation = tutorialPointerPositions[currentMissionNum].rotation;
                
                
                break;
            case 5:
            currentPointerArrow.transform.localPosition = new Vector3(0,1000f,1000f);
                StartCoroutine(StopVoicePointer());
                break;
            case 9:
                //allNode = GameObject.FindGameObjectsWithTag("Node");
                //lastNode = allNode[allNode.Length - 1];


                currentPointerArrow.transform.localPosition = lastNode.transform.position.normalized*6f;
                currentPointerArrow.transform.parent = lastNode.transform;
                //currentPointerArrow.transform.localPosition = Vector3.zero;
               

                currentPointerArrow.transform.localRotation = Quaternion.identity;

                currentPointerArrow.transform.Translate(0f, -0.6f, 0f, Space.Self);
                currentPointerArrow.transform.Rotate(0f, 0f, 90f, Space.Self);

                break;

            case 10:
                currentPointerArrow.transform.parent = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MenuContainer>().transform.GetChild(3).transform;
                currentPointerArrow.transform.localPosition = tutorialPointerPositions[currentMissionNum].position;

                currentPointerArrow.transform.localRotation = tutorialPointerPositions[currentMissionNum].rotation;

                break;
            case 11:
                currentPointerArrow.transform.parent = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MenuContainer>().transform.GetChild(4).transform;
                currentPointerArrow.transform.localPosition = tutorialPointerPositions[currentMissionNum].position;

                currentPointerArrow.transform.localRotation = tutorialPointerPositions[currentMissionNum].rotation;

                break;
            case 13:
                currentPointerArrow.transform.parent = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MenuContainer>().transform.GetChild(0).transform;
                currentPointerArrow.transform.localPosition = tutorialPointerPositions[currentMissionNum].position;

                currentPointerArrow.transform.localRotation = tutorialPointerPositions[currentMissionNum].rotation;

                break;
            default:
                currentPointerArrow.transform.position = tutorialPointerPositions[currentMissionNum].position;
                currentPointerArrow.transform.rotation = tutorialPointerPositions[currentMissionNum].rotation;
                break;
        }
    }

    public void PointArrowHover(bool isHovered)
    {
        if (isHovered != hoverErrorProtect) return;
        else
        {
            switch (currentMissionNum)
            {

                case 4:
                    if (isHovered)
                    {
                        currentPointerArrow.transform.Translate(1.3f, 0, 0, Space.Self);
                        currentPointerArrow.transform.Rotate(0, 180, 0, Space.Self);
                    }
                    else
                    {
                        currentPointerArrow.transform.Translate(1.3f, 0, 0, Space.Self);
                        currentPointerArrow.transform.Rotate(0, 180, 0, Space.Self);

                    }
                    break;
                case 7:
                    if (isHovered)
                    {
                        currentPointerArrow.transform.Translate(0.7f, -0.27f, 0, Space.Self);
                        currentPointerArrow.transform.Rotate(0, 0, 180, Space.Self);
                    }
                    else
                    {
                        currentPointerArrow.transform.Rotate(0, 0, -180, Space.Self);
                        currentPointerArrow.transform.Translate(-0.7f, 0.27f, 0, Space.Self);
                    }
                    break;
                case 9:
                    if (isHovered)
                    {
                        currentPointerArrow.transform.Translate(0.7f, 0.27f, 0, Space.Self);
                        currentPointerArrow.transform.Rotate(0, 0, -180, Space.Self);
                    }
                    else
                    {
                        currentPointerArrow.transform.Rotate(0, 0, 180, Space.Self);
                        currentPointerArrow.transform.Translate(-0.7f, -0.27f, 0, Space.Self);
                    }
                    break;

                default:

                    break;
            }
            hoverErrorProtect = !hoverErrorProtect;
        }

        


    }

    IEnumerator StopVoicePointer()
    {
        yield return new WaitForSeconds(2f);
        currentPointerArrow.transform.position = tutorialPointerPositions[currentMissionNum].position;
        currentPointerArrow.transform.rotation = tutorialPointerPositions[currentMissionNum].rotation;
    }
}
