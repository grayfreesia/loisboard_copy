﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TutorialSkip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler,IGvrPointerHoverHandler
{
    public float gazeTime = 1.5f;
    private float _timer = 0f;
    private Image _progressImg;
    private GameObject _currentHoverButton;
    private bool _shouldGazeAgain = false;

    public void OnPointerEnter(PointerEventData ped)
    {
        //_timer = 0f;
        _currentHoverButton = ped.pointerEnter.transform.parent.gameObject;
        _progressImg = ped.pointerEnter.GetComponent<Image>();
        //_progressImg.fillAmount = 1f;
        iTween.ScaleTo(_currentHoverButton, Vector3.one * 1.2f, 1f);
    }

    public void OnPointerExit(PointerEventData ped)
    {
        _timer = 0f;
        _progressImg.fillAmount = 1f;
        _shouldGazeAgain = false;
        iTween.ScaleTo(_currentHoverButton, Vector3.one, 1f);
    }

    public void OnGvrPointerHover(PointerEventData ped)
    {
        if (_shouldGazeAgain)
        {
            return;
        }

        _timer += Time.deltaTime;
        _progressImg.fillAmount = 1f - (_timer / gazeTime);

        if (_timer >= gazeTime)
        {
            //  실행
            StartCoroutine(TutorialManager.Instance.EndTutorial());

            _progressImg.fillAmount = 1f;
            _timer = 0f;
            iTween.ScaleTo(_currentHoverButton, Vector3.one, 1f);
            _shouldGazeAgain = true;
        }
    }
}
