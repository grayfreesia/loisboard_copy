﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointArrowAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        iTween.MoveBy(gameObject, iTween.Hash("x", 0.1f, "time", 0.3f, "loopType", "pingPong"
            , "delay", 0f, "easeType", "easeInOutQuad"));
    }
	

}
